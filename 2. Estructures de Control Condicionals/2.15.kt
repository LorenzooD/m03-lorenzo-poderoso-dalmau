package Conditions

import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)

    print("Insert a letter: ")
    val character : Char = scanner.next().single()

    when (character){
        'a', 'e', 'i', 'o', 'u' -> println("Es vocal")
        'A', 'E', 'I', 'O', 'U', -> println("Es vocal")
        else -> println("No es vocal")
    }
}