package Bucles

import java.util.*

fun main (){

    var scanner = Scanner(System.`in`)

    print("¿Que tabla de multiplicar quieres? ")
    var input = scanner.nextInt()

    // "En el for le asignamos a "i" un rango del 1 al 10"
    for (i in 1..10) {
        //Creamos variable que lo multiplicará por "i"
        val product = input * i
        //Imprimimos el valor de entrada "input" por el rango "i" que será el resultado de la variable que los multiplica "product"
        println("$input * $i = $product")

    }
}