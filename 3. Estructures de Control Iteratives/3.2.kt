package Bucles

import java.util.*


fun main(){

    var scanner = Scanner(System.`in`)

    println("Enter the number: ")
    var input = scanner.nextInt()
    var sum = 0
    var i = 1

    while (i <= input) sum += i++
    println("$sum")
}

/*
Lo que hacemos con este while es que el while use como condición que
la variable "i" sea menor o igual al "input". Mientras no lo sea, se irá
acumulando en la variable "sum"
 */
