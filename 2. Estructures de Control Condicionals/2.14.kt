package Conditions

import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)

    print("Insert a year: ")
    val year = scanner.nextInt()

    if (year%4 == 0) println("Es bisiesto")
    else println("No es bisiesto")
}