package Conditions

import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)

    print("Insert a number : ")
    val number = scanner.nextInt()

    if (number > 0 ) println(number)
    else if (number < 0) println(-number)

}