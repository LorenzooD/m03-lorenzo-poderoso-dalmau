import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)

    println("Introduce un valor: ")
    val lineas = scanner.nextInt()

    // Primero creamos la mitad del rombo
    for (i in 1 until lineas) { //Este for indicará las líneas (Tamaño) del rombo hasta la mitad
        for(k in i..(lineas-1)) // Este for indica los espacios de cada línea. Es decir, si el tamaño es 5, hará 5 espacios -1 (Ya que el 5to char será el (*)
        {
            print(" ")
        }

        for (j in 1..2*i-1) { // Este  for imprime los (*) que dibujarán el rombo.
            print("*")
        }
        println()
    }

    // Hacemos lo mismo pero a la inversa
    for (i in lineas downTo 1) {
        for(k in i..(lineas-1))
        {
            print(" ")
        }

        for (j in 1..2*i-1) {
            print("*")
        }
        println()
    }
}