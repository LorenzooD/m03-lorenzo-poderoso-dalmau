import java.util.*

/*
* AUTHOR: Lorenzo Poderoso Dalmau
* DATE: 19/11/2022
* TITLE: Inverteix les paraules (3)
*/


fun main(){
    val scanner = Scanner(System.`in`)
    val palabra = scanner.nextLine()

    println(palabra.reversed())
}