import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)

    println("Introduzca un valor: ")
    var num = scanner.nextInt()
    var result = 1L

    for (i in 1 ..  num){
        var j = 2
        while (j < i && i%j != 0)j++
            if (i==j) result *= j
    }
    println(result)
}