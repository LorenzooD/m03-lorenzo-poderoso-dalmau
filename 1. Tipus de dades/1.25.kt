import java.util.*

/*
* AUTHOR: Lorenzo Poderoso Dalmau
* DATE: 03/10/2022
* TITLE: Fes-me majúscula
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introduce una letra: ")
    var letra = scanner.next().single()

    println(letra.toUpperCase()) //toUpperCase transforma la letra minúscula a mayúscula
}