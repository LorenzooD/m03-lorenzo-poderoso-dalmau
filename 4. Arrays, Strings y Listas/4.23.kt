import java.util.*

/*
* AUTHOR: Name Surname1 Surname2
* DATE: year/moth/day
* TITLE: Invirteix les paraules
*/


fun main(){
    val scanner = Scanner(System.`in`)
    println("Introduzca una palabra: ")
    val palabra1 = scanner.next()
    println("Introduzca otra palabra: ")
    val palabra2 = scanner.next()
    println("Introduzca otra palabra: ")
    val palabra3 = scanner.next()
    
    for (i in palabra1.lastIndex downTo 0){
        print(palabra1[i])
    }
    println()
    for (i in palabra2.lastIndex downTo 0){
        print(palabra2[i])
    }
    println()
    for (i in palabra3.lastIndex downTo 0){
        print(palabra3[i])
    }
}