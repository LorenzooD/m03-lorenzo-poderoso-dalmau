import java.util.*

/*
* AUTHOR: Lorenzo Poderoso Dalmau
* DATE: 06/10/2022
* TITLE: Equacions de segon grau
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introduce un número: ")
    val a = scanner.nextInt()
    print("Introduce un número: ")
    val b = scanner.nextInt()
    print("Introduce un número: ")
    val c = scanner.nextInt()

    var numero1 = (-b - Math.sqrt(((b * b) - 4 * a * c).toDouble())) / (2 * a)
    var numero2 = (-b + Math.sqrt(((b * b) - 4 * a * c).toDouble())) / (2 * a)

    println(numero1)
    println(numero2)
}