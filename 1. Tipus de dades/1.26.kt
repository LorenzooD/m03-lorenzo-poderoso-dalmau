import java.util.*

/*
* AUTHOR: Lorenzo Poderoso Dalmau
* DATE: 03/10/2022
* TITLE: Fes-me minúscula
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introduce una letra: ")
    var letra = scanner.next().single()

    println(letra.toLowerCase())
}