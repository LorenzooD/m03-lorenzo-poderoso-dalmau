import java.util.*
/*
* AUTHOR: Lorenzo Poderoso Dalmau
* DATE: 28/11/2022
* TITLE: Dígits centrals
*/

//No está realizado lo que pide. ¡CAMBIARLO!

fun main(){

    //VARIABLES
    val scanner = Scanner(System.`in`)
    println("Introduzca los intentos: ")

    //VARIABLES
    val listAnna = mutableListOf<Int>()
    val listBernat = mutableListOf<Int>()
    val intents = scanner.nextInt() // Intentos que tendrá el juego


    //ESTE BUCLE ACUMULA TODOS LOS NÚMEROS INTRODUCIDOS EN LA LISTA MUTABLE DE CADA UNO
    println("***COMIENZA EL JUEGO***")
     for (i in 0 until intents) {
         print("Anna: ")
         val anna = scanner.nextInt()
         listAnna.add(anna)

         print("Bernat: ")
         val bernat = scanner.nextInt()
         listBernat.add(bernat)
     }
    val letrasRepetidas = mutableListOf<Int>()
    //COMPROBACIÓN DE NUMEROS IGUALES
    var count = 0
    for (i in 0 until listAnna.lastIndex){
        for (j in 0 until listBernat.lastIndex){
            if (listAnna[i] !in letrasRepetidas)letrasRepetidas.add(listAnna[i])
            else if (listBernat[j] !in letrasRepetidas)letrasRepetidas.add(listBernat[j])
            else if (listAnna[i] == listBernat[j]) count++
        }
    }

    //COMPROBACIÓN

    println(listAnna)
    println(listBernat)
    println(count)
}


