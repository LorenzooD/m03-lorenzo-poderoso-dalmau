import java.util.*

/*
* AUTHOR: Name Surname1 Surname2
* DATE: year/moth/day
* TITLE: Palíndrom
*/


fun main(){
    val scanner = Scanner(System.`in`)
    println("Introduzca una palabra: ")
    val palabra = scanner.next()

    if (palabra == palabra.reversed()) println("És un palíndrom")
    else println("No és un palíndrom")

}
