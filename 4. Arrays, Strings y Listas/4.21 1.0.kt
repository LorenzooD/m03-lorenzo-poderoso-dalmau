import java.util.*

/*
* AUTHOR: Lorenzo Poderoso Dalmau
* DATE: 16/11/2022
* TITLE: Parèntesis
*/

fun main(){

    //VARIABLES
    val scanner = Scanner(System.`in`)
    val entrada = scanner.next()
    var count = 0

    for (i in entrada){
        if (entrada == "(")count++
        else if (entrada == ")")count--
    }

    if (count == 0) println("Si")
    else println("No")

}
