import java.util.*

/*
* AUTHOR: Lorenzo Poderoso Dalmau
* DATE: 29/11/2022
* TITLE: Files i columnes
*/

fun main(){
    //Matriz
    val matrix: MutableList<MutableList<Int>> = mutableListOf()
    val resultColumna = mutableListOf<List<Int>>()

    //ENTRADA
    val scanner = Scanner(System.`in`)
    println("Introduzca las columnas de la matriz: ")
    val columnas = scanner.nextInt()
    println("Introduzca las filas de la matríz: ")
    val filas = scanner.nextInt()


    //-----Petición de valores para la matríz-----//
    println("Introduzca los valores de su matriz: ")
    for (i in 0 until columnas){
        matrix.add(mutableListOf())
        for (j in 0 until filas){
            val valores = scanner.nextInt()
            matrix[i].add(valores)
        }
    }

    //CONSULTA
    println("¿Que fila quieres ver? ")
    val fila = scanner.nextInt()
    println("¿Que columna quieres ver? ")
    val columna = scanner.nextInt()
    println("Que elemento de la fila y columna quiere ver? ")
    val elementoFila = scanner.nextInt()
    val elementoColumna = scanner.nextInt()
    println("¿Que otra fila quieres ver? ")
    val fila2 = scanner.nextInt()
    //VARIABLES RESULTADO


    //---------LOGICA---------//
    /*
        j:  j:  j:
    i:  1   2   3
    i:  4   5   6
    i:  7   8   9

    j i
    0 0
    1 0
    2 0

     */
    var aux = 0
    //Obtención de datos de la columna deseada
    for (i in 0 .. matrix.lastIndex){
        val solidColumn = mutableListOf<Int>()
        for (j in 0 .. matrix.lastIndex){
            aux = matrix[j][i]
            solidColumn.add(aux)
        }
        resultColumna.add(solidColumn)
    }

    //




    //SALIDA
    println("Fila $fila: ${matrix[fila-1]}")
    println("Columna $columna: ${resultColumna[columna-1]}")
    println("Elemento $elementoFila $elementoColumna: ${matrix[elementoFila - 1][elementoColumna - 1]}")
    println("Fila $fila2: ${matrix[fila2-1]}")

}