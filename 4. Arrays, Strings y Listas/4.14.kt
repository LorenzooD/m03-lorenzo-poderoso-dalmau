import java.util.*

fun main(args: Array<String>){

    val scanner = Scanner(System.`in`)
    println("Introduzca un valor: ")
    val num = scanner.nextInt()

    for (i in args){
        for (j in args){
            if (i.toInt() + j.toInt() == num) println("$i $j")
        }
    }
}