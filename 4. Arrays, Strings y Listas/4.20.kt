import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)

    println("Insert the first ADN: ")
    val firstADN = scanner.next()
    println("Insert the second ADN: ")
    val secondADN = scanner.next()

    var count = 0

    if (firstADN.lastIndex != secondADN.lastIndex) println("Entrada no valida. ")

    for (i in 0.. firstADN.lastIndex){
        if (firstADN[i] != secondADN[i]) count++
    }
    println("ADNs Introduïts: ")
    println()
    println("*****************")
    println(firstADN)
    println(secondADN)
    println("*****************")
    println()
    println("Els 2 ADNs tenen: $count diferencies.")
    println()
    println()
}