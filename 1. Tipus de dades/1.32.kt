import java.util.*

/*
* AUTHOR: Lorenzo Poderoso Dalmau
* DATE: 13/10/2022
* TITLE: Calcula el capital (no LA capital)
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introduzca el  capital: ")
    var capital = scanner.nextInt()
    print("Introduzca los años: ")
    var years = scanner.nextInt()
    print("Introduzca los intereses: ")
    var interests = scanner.nextInt()

    var finalCapital = ((capital * interests/100) * years) + capital

    println(finalCapital)



}