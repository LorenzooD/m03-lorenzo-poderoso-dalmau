import java.util.*
/*
* AUTHOR: Lorenzo Poderoso Dalmau
* DATE: 29/11/2022
* TITLE: Suma de matrius
*/

fun main(){

    //Variables
    val scanner = Scanner(System.`in`)
    println("Size of matrix: ")
    val size = scanner.nextInt()

    //Matrices
    val firstMatrix: MutableList<MutableList<Int>> = mutableListOf()
    val secondMatrix: MutableList<MutableList<Int>> = mutableListOf()
    val result: MutableList<MutableList<Int>> = mutableListOf()

    //first matrix
    for (i in 0 until size){
        firstMatrix.add(mutableListOf())
        for (j in 0 until size){
            val number = scanner.nextInt()
            firstMatrix[i].add(number)
        }
    }
    //second matrix
    for (i in 0 until size){
        secondMatrix.add(mutableListOf())
        for (j in 0 until size){
            val number = scanner.nextInt()
            secondMatrix[i].add(number)
        }
    }

    //resultado
    for (i in 0 until size){
        result.add(mutableListOf())
        for (j in 0 until size){
            result[i].add(firstMatrix[i][j] + secondMatrix[i][j])
        }
    }

    println("First Matrix: $firstMatrix")
    println("Second Matrix: $secondMatrix")
    println("Result: $result")
}

/*
1 2 3
4 5 6 -- i
7 8 9
  |
  j

9 8 7 -- i
6 5 4
3 2 1
  |
  j
 */
