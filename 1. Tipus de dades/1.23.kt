import java.util.*

/*
* AUTHOR: Lorenzo Poderoso Dalmau
* DATE: 03/10/2022
* TITLE: És una lletra?
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introduce una letra: ")
    var letra = scanner.next().single()



    println(letra.code > 64 && letra.code < 91 || letra.code > 96 && letra.code < 123)
}