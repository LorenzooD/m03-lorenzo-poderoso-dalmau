import java.util.*

/*
* AUTHOR: Lorenzo Poderoso Dalmau
* DATE: 06/10/2022
* TITLE: Quant de tems?
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introduce un número: ")
    var seconds = scanner.nextInt()

    val hour = (seconds / 3600) // Dividimos segundos entre 3600 que son los segundos que tiene 1 hora
    val minutes = (seconds / 60) % 60 //Luego lo dividimos entre 60 que son los segundos de un minuto
    seconds %= 60 //Sacamos el modulo de 60 que es 0 para printar los 0 segundos

    println("$hour hora $minutes minuts $seconds segons")

}