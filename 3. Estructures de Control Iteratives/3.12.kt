import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)

    println("Introduzca un valor: ")
    val num = scanner.nextInt()

    for (i in 1..num){
        if (i % 3 == 0 && i % 5 == 0){
            println(" $i es divisible por 3 y 5")
        }
        else if (i % 3 == 0){
            println("$i es divisible por 3")
        }
        else if (i % 5 == 0){
            println("$i es divisible por 5")
        }
    }
}