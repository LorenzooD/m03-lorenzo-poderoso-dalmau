import java.util.*

fun main(args: Array<String>){

    var resultado = 0.0
    var valor = 0.0

    for (arg in args){
        val nombre = arg.toInt()
        resultado += nombre
        ++valor
    }
    println(resultado/valor)
}

/*
Creo una variable resultado que será el calculo final. Como se imprime
con decimales el valor de dentro de la variable valor y resultado estan
con decimal "0.0"

Después he creado un bucle en el que dentro creo una variable nombre donde coge
numero por numero. Después hago que cada número de dentro se vaya sumando entre ellos
y a su vez por cada vez que se sume se va sumando 1 a la variable valor.

Si el programa hace el calculo 9 veces, a "valor" se le sumará 1 hasta que llegue a 9
Finalmente en el print divido la suma de todos los números (44) entre las 9 veces que el
programa ha sumado (9).
(44/9= 4.888888888888889)
 */