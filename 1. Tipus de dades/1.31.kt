import java.util.*

/*
* AUTHOR: Lorenzo Poderoso Dalmau
* DATE: 13/10/2022
* TITLE: De metre a peu
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introduce un número: ")
    val metros = scanner.nextInt()
    val pies = (39.37 / 12) * metros

    println(pies)

}