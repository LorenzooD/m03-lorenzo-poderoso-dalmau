import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)

    println("Introduzca un valor: ")
    val lineas = scanner.nextInt()

    for (i in lineas downTo 1){
        for (j in 1..i){
            print("*")
        }
        println()
    }
}