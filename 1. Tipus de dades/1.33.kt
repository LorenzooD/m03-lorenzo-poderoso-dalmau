import java.util.*

/*
* AUTHOR: Lorenzo Poderoso Dalmau
* DATE: 13/10/2022
* TITLE: És divisible
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introduzca el primer número: ")
    var firstNumber = scanner.nextInt()
    print("Introduzca el segundo número: ")
    var secondNumber = scanner.nextInt()

    println((secondNumber%firstNumber)==0)

}