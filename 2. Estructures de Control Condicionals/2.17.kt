package Conditions

import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)

    print("Insert a numbers : ")
    val number1 = scanner.nextInt()
    val number2 = scanner.nextInt()
    val number3 = scanner.nextInt()

    if (number1 < number2 && number1 < number3) println("Ascendente")
    if (number1 > number2 && number1 > number3) println("Descendente")
    else println("Ninguna de las dos")
}