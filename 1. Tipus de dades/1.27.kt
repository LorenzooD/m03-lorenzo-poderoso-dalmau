import java.util.*

/*
* AUTHOR: Lorenzo Poderoso Dalmau
* DATE: 04/10/2022
* TITLE: Som iguals?
*/

fun main() {
    val scanner = Scanner(System.`in`)

    print("Introduce una letra: ")
    val l1 = scanner.next().single()
    print("Introduce otra letra: ")
    val l2 = scanner.next().single()

    println(l1.lowercaseChar() == l2.lowercaseChar())


}